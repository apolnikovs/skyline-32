<script type="text/javascript">
$(document).ready(function() {
    $("#UnitTypeID").combobox();      
        $("#AccessoriesForm").validate( {    
       ignore: '',
        rules: {  
               AccessoryName: "required",
               UnitTypeID:
                        {
                         required: true
                         }   
                },               
        errorPlacement: function(error, element){   
                            error.insertAfter(element);
                    },
          submitHandler: function(form) {
                $('#loading').show();    
                $(form).ajaxSubmit({
                            url:"{$_subdomain}/ProductSetup/saveAccessory",
                            type:"post",
                            success: function(){                             
                              $('#AccessoriesFormPanel').hide();
                              {if $datarow.AccessoryID neq '' && $datarow.AccessoryID neq '0'}                                  
                              $('#dataUpdatedMsg').show();   
                              window.location.href="{$_subdomain}/ProductSetup/accessory/";
                              {else}
                              $('#dataInsertedMsg').show(); 
                              window.location.href="{$_subdomain}/ProductSetup/accessory/";
                              {/if}
                              parent.$.colorbox.resize({
                                              width:400,
                                              height:275
                                            });
                                            
                      }
                    });
          },        
        
             errorContainer: '#error-notify',   
             errorClass: 'fieldError',
        } );
      $('input:visible:enabled:first').focus(); 
      
    //help text
    $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
	//help text end  
}); 

function IntializeDatatable(){
$.colorbox.close();
}
</script>
   
   <div id="AccessoriesFormPanel" class="SystemAdminFormPanel" >
   
               <form id="AccessoriesForm" name="AccessoriesForm" method="post" action="{$_subdomain}/ProductSetup/saveAccessory" class="inline" >
      
               <fieldset>
                   <legend title="{$page['Text']['form_title']|escape:'html'}" >{$page['Text']['form_title']|escape:'html'}</legend>
                       
                   <p><label id="suggestText" ></label></p>
                           <p>
                           <label ></label>
                           <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                           </p>
                      
                      
                        <p>
                           <label class="cardLabel" for="AccessoryName" >{$page['Labels']['accessory_name']|escape:'html'}:<sup>*</sup></label>
                          
                            &nbsp;&nbsp; <input type="text" class="text"  name="AccessoryName" value="{$datarow.AccessoryName|escape:'html'}" id="AccessoryName" >
                       
                        </p>
                        <p>
                           <label class="cardLabel" for="UnitTypeID" >{$page['Labels']['unit']|escape:'html'}:<sup>*</sup></label>
                          
                            &nbsp;&nbsp;  
                            <select name="UnitTypeID" id="UnitTypeID" class="text" >
                               <option value="" {if $datarow.UnitTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                               {foreach $unitTypes as $unitType}

                                   <option value="{$unitType.UnitTypeID}" {if $datarow.UnitTypeID eq $unitType.UnitTypeID}selected="selected"{/if}>{$unitType.UnitTypeName|escape:'html'}</option>

                               {/foreach}
                               
                           </select>
                           <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AccessoryFormUnitTypeHelp" class="helpTextIconQtip" style="margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" > 
                         </p>
                        
                         <p>
                                <label class="cardLabel" for="Status">{$page['Labels']['status']|escape:'html'}:</label>
                                &nbsp;
                                <span class="formRadioCheckText">
                                    
	<input type="checkbox" name="Status" value="In-active"  {if isset($datarow.Status) &&  ($datarow.Status eq 'In-active') } checked="checked"  {/if}  />
<label>Inactive</label>&nbsp;
					</span>

                                    
          </p>
                         
                        <!-- comment by srinivas 
                        {if $datarow.AccessoryID neq '' && $datarow.AccessoryID neq '0'}
                            
                         <p>
                           <label class="cardLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                          
                            &nbsp;&nbsp; 
                       
                               {foreach $statuses as $status}

                                   <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                               {/foreach}    

                         </p>
                         
                        {/if}
                        comment end -->
                        <input type="hidden" name="AccessoryID" value="{$datarow.AccessoryID}">
                        <input type="hidden" name="UnitTypeIDHidden" value="{$datarow.UnitTypeID}">

                        <p>
               <hr>
                              
                              <div style="height:20px;margin-bottom: 10px;text-align: center;">
                                
                                <button type="submit" style="margin-left:38px" class="gplus-blue centerBtn" id="save_btn" name="save_btn"> {$page['Buttons']['save']|escape:'html'} </button>
                                <span id="loading" style="display:none"> <img src="{$_subdomain}/images/ajax-loader.gif" > </span>  
                                <button type="button" onclick="$.colorbox.close();" class="gplus-blue" style="float:right"> {$page['Buttons']['cancel']|escape:'html'} </button>
                                </div>
           </p>    

                        


               </fieldset>    
                       
               </form>        
                       
      
</div>

{* This block of code is for to display message after data updation *}                                      
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="{$page['Text']['form_title']|escape:'html'}" >{$page['Text']['form_title']|escape:'html'}</legend>
                <p>
                 
                    <b> Your data has been updated successfully.</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <button type="button" onclick="IntializeDatatable();" class="gplus-blue"> ok </button>

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                 <legend title="{$page['Text']['form_title']|escape:'html'}" >{$page['Text']['form_title']|escape:'html'}</legend>
                <p>
                 
                    <b>Your data has been inserted successfully.</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <button type="button" onclick="$.colorbox.close();" class="gplus-blue" > ok </button>

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div> 
