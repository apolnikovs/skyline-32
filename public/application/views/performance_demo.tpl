{extends "DemoLayout.tpl"}

{block name=config}

{$PageId = $PerformancePage}
{/block}

{block name=scripts}

{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    
{/block}

{block name=body}



    

    
<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']}</a> / {$page['Text']['page_title']}
    </div>
</div>
    
{include file='include/menu.tpl'}

        
{* <div class="main" id="performance">   
   
   <h2>This page is under development...</h2>
                                    
</div> *}


<iframe src="{$_subdomain}/demo/{$tab}.html" width="100%" height="680" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>

{/block}
