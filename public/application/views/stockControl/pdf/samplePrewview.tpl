<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<style>
.telegistics{
	/*font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif;*/
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
.orange{
	color:#{$data[0]['DocColour1']};
	margin:0px;
	font-weight:bold;
	text-transform:uppercase;
}
</style>
</head>

<body>
<table class="telegistics" width="610" height="100" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="300" rowspan="3"><img src="{if $data[0]['DocLogo']!=""}{$_subdomain}/images/serviceProvidersLogos/{$data[0]['DocLogo']}{else}{$_subdomain}/images/skylineDocLogo.png{/if}"  alt="logo" /></td>
    <td width="260"><h1 class="orange">Stock Order</h1></td>
  </tr>
  <tr>
    <td>Requisition Number: <span style="font-weight:bold;">2368</span></td>
  </tr>
  <tr>
    <td>Date Printed: <span style="font-weight:bold;">11/03/2013</span></td>
  </tr>
</table>
<!--horizontal line -->
<table style="background:#{$data[0]['DocColour2']}; height:5px;width:700px;" >
<tr></tr>
</table>
<!--addres table -->
<table class="telegistics" width="610" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
  <tr>
    <td><h3 class="orange">SUPPLIER ADDRESS</h3></td>
    <td><h3 class="orange">DELIVERY ADDRESS</h3></td>
  </tr>
  <tr>
    <td>NOKIA RF TELECOMMUNICATIONS LT</td>
    <td>TELEGISTICS</td>
  </tr>
  <tr>
    <td>WHITMORE HOUSE</td>
    <td>PRIVATE BAG 92506</td>
  </tr>
  <tr>
    <td>6 LONDON RD, ASCOT</td>
    <td>WELLESLEY STREET</td>
  </tr>
  <tr>
    <td>BERKSHIRE</td>
    <td>AUCKLAND 1141</td>
  </tr>
  <tr>
    <td>SL5 8DH</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Tel: (+44)1344886909</td>
    <td>Tel: 0800 83534477</td>
  </tr>
  <tr>
    <td>Tel: (+44)1344886936</td>
    <td>Tel: (09) 2585401</td>
  </tr>
</table>
<!--horizontal line -->
<table style="background:#{$data[0]['DocColour2']}; height:5px;width:700px;margin-top:5px;" >
<tr></tr>
</table>

<!--description table -->
<table class="telegistics" border="0" cellpadding="1" cellspacing="0"  style="width:700px;margin-top:25px;">
<tr style="background:#{$data[0]['DocColour2']}; height:15px;width:700px;margin-top:25px;#{$data[0]['DocColour3']}">
<th style="color:#{$data[0]['DocColour3']};text-align: left;" width="8%">QTY</th>
<th style="color:#{$data[0]['DocColour3']};text-align: left;" width="20%">PART NUMBER</th>
<th style="color:#{$data[0]['DocColour3']};text-align: left;" width="37%">PART DESCRIPTION</th>
<th style="color:#{$data[0]['DocColour3']};text-align: left;" width="15%">ITEM COST</th>
<th style="color:#{$data[0]['DocColour3']};text-align: left;" width="20%">LINE COST</th>
</tr>
</table>
<table class="telegistics" border="0" cellpadding="1" cellspacing="0" style="width:610px;font-size:11px;">
<tr>
<td width="8%">25</td>
<td width="23%">0073081</td>
<td width="23%">5510 NPM-5 SWAP(old 00770407)</td>
<td width="23%">230.36</td>
<td width="23%">115,759.00</td>
</tr>
<tr>
<td width="8%">26</td>
<td width="23%">0073081</td>
<td width="23%">5510 NPM-5 SWAP(old 00770407)</td>
<td width="23%">70.36</td>
<td width="23%">15,759.00</td>
</tr>
<tr>
<td width="8%">27</td>
<td width="23%">0073081</td>
<td width="23%">5510 NPM-5 SWAP(old 00770407)</td>
<td width="23%">40.36</td>
<td width="23%">5,759.00</td>
</tr>
<tr>
<td width="8%">28</td>
<td width="23%">0073081</td>
<td width="23%">5510 NPM-5 SWAP(old 00770407)</td>
<td width="23%">230.36</td>
<td width="23%">22,759.00</td>
</tr>
</table>
<!-- summary table -->
<table class="telegistics" width="610" border="0" cellspacing="0" cellpadding="5" style="margin-top:25px;">
  <tr>
    <td width="50%"><h3 class="orange">SUMMARY</h3></td>
    <td width="50%"><h3 class="orange">AUTHORISED</h3></td>
  </tr>
  <tr>
  <td>Total lines: <span style="font-weight:bold;">4</span></td>
  <td>Name: <span>........................................................................</span></td>
  </tr>
  <tr>
  <td>Total Items: <span style="font-weight:bold;">110</span></td>
  <td>Position: <span>.....................................................................</span></td>
  </tr>
  <tr>
  <td>Total Order Value: <span style="font-weight:bold;">263,589.00 NZD</span></td>
  <td>Signature: <span>..................................................................</span></td>
  </tr>
  <tr>
  <td><span style="font-weight:bold;margin-left:100px;">145,807.20 GBP</span></td>
  <td>Date: <span>..........................................................................</span></td>
  </tr>
  </table>
</body> 