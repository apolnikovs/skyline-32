{if !isset($appID)}
    {$appID=10}
    {/if}
   
   {if $mapCount=0}{$divider=0}{/if}
   
<html>
    <head>
     
        <script>
            var mylatlong=0;
            var defLat=0;
            var defLong=0;
            var mapa1;
            var mapa2;
            var mapa3;
            var mapa4;
            var mapa5;
            var mapa6;
            var zoomlevel=11;
           
            $(document).ready(function() {
            
            setTimeout(function(){
    
  
        
        
    {if isset($adress)}
     var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address' : "{$adress}, UK"}, function(results, status){
  
     defLat = results[0].geometry.location.lat();
     defLong =results[0].geometry.location.lng() ;
     mylatlong=defLat+','+defLong;
    

{else}
    
 var defLat = 52.23793;
    var defLong = -0.90267 ;
    var mylatlong=defLat+','+defLong;
{/if}
    
    {if isset($appGeoTag)}
         defLat = {$appGeoTag.Latitude};
     defLong ={$appGeoTag.Longitude};
    mylatlong=defLat+','+defLong;
     
        {/if}
          
    var customMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/checkered_flag.png",new google.maps.Size(64, 64),  new google.maps.Point(0, 0),new google.maps.Point(6, 64));
    var TAMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/pointer3.png",new google.maps.Size(50, 60),  new google.maps.Point(0, 0),new google.maps.Point(25, 60));
    var amMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_b_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var pmMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_g_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var anyMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_y_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var amTMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_bt_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var amSMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_bs_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var pmSMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_gs_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var pmTMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_gt_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var atTMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_yt_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var atSMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_ys_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    //var unreachedMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/1363794581_gnome-unknown.png",new google.maps.Size(36, 36),  new google.maps.Point(0, 0),new google.maps.Point(18, 18));
    var unreachedMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/unreachable.png",new google.maps.Size(52, 52),  new google.maps.Point(0, 0),new google.maps.Point(26, 26));
    var errorMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/error2.png",new google.maps.Size(52, 52),  new google.maps.Point(0, 0),new google.maps.Point(26, 26));
        
         
   
   
    {$c=1}
        {$dates=array()}
           
   {for $o=0 to $mapCount}
        var infowindow{$o+1} = new google.maps.InfoWindow();
   var    mapa{$o+1}= $('#mapgoogle{$o+1}').gmap({ 'center':  mylatlong, 'zoom':12}).bind('init', function(event, map) { 
   {if isset($adress)}
	$('#mapgoogle{$o+1}').gmap('addMarker', { 'position': mylatlong, icon:customMarker,
    
'draggable': true , 
			'bounds': false,
                        id:'mapMarker{$o+1}',
                        title:"{if isset($adress)}{$adress}{/if}",
                        MAX_ZINDEX:1000
		}, function(map, marker) { 
               
            $.post("{$_subdomain}/Diary/setSessionGeoTag",{ 'lat':defLat,'lng':defLong});
                google.maps.event.addListener(marker, 'dblclick', function(event){
   
    map.setMapTypeId(google.maps.MapTypeId.HYBRID);
    map.setCenter(marker.getPosition()); // set map center to marker position
    smoothZoom(map, 19, map.getZoom()); // call smoothZoom, parameters map, final zoomLevel, and starting zoom level
    
})
  			//$('#dialog').append('');
			//findLocation(marker.getPosition(), marker,$('#map{$o+1}'));
		}).dragend( function(event) {
			findLocation(event.latLng, this,$('#mapmapgoogle{$o+1}'));
		}).click( function() {
               
		
});//adding current app marker
{/if}
        });
  
        {/for}
            

            
             {foreach from=$waypoints key=k item=v}
         {$dates[]=$k}
        {if $c<7}
            {$skip=0}
   var LatLngList{$c} =new Array();
   
   {for $e=0 to $v|@count-1}
       
       {if isset($v[$e].latitude)}
          
   LatLngList{$c}[{$e}]=new google.maps.LatLng ({$v[$e].latitude},{$v[$e].longitude})
   //ading others markers
   
   {$marker=""}
       {if $v[$e].Unreached==1} 
     {$marker=unreachedMarker}
    {elseif $v[$e].Error==""}
          {$marker=errorMarker}
            
        {else}
        {if $v[$e].ta===1}icon:TAMarker,{/if}
                {if $v[$e].AppointmentTime=="ANY"}
                    
                {if $v[$e].manufacturer=="SAMSUNG" || $v[$e].manufacturer=="Samsung"}
                    {$marker=atSMarker}
                {elseif $v[$e].ta==1}
                    {$marker=atTMarker}
            {else}
                {$marker="anyMarker"}{/if}{/if}
                {if $v[$e].AppointmentTime=="AM"}
                    {if $v[$e].manufacturer=="SAMSUNG"||$v[$e].manufacturer=="Samsung"}
                    {$marker=amSMarker}
             {elseif $v[$e].ta===1}
                    {$marker=amTMarker}
            {else}    
            {$marker="amMarker"}{/if}{/if}
                {if $v[$e].AppointmentTime=="PM"}
                    {if $v[$e].manufacturer=="SAMSUNG"||$v[$e].manufacturer=="Samsung"}
                    {$marker=pmSMarker}
             {elseif $v[$e].ta===1}
                    {$marker=pmTMarker}
            {else}        
            {$marker="pmMarker"}{/if}{/if}
   {/if}
    $('#mapgoogle{$c}').gmap('addMarker', { 'position': '{$v[$e].latitude},{$v[$e].longitude}',
   icon:{$marker},
    MAX_ZINDEX:100,
    popup:true
    }, function(map, marker) {
			google.maps.event.addListener(marker, 'click', function(){
                       var content = '<br> <table style="background:none">';
       content+='<tr><td style="background:none"><span style="float:rigth;width:120px">Customer:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].name}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Time window:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].twindow}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Service Time:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].duration|date_format:"%H:%M"}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Manufacturer:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].manufacturer|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Engineer Name:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].engname|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Product Type:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].utName|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Time Slot:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].AppointmentTime|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Skill Type:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].reptype|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Appointment Type:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].AppointmentType|escape:'html'}</span></td></tr>';
       content+='</table><hr><span style="float:rigth">Address:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].adress|escape:'html'}</span>';
       infowindow{$c}.setContent(content);
                // z.log(map);
                 infowindow{$c}.maxWidth=350;
   infowindow{$c}.open(map,this);
    });                                           
                       
		 
});
       
        {elseif  $v|@count<=1}{$skip=1}{/if}
   {/for}
       
      // console.log(defLong);
    LatLngTarget=new google.maps.LatLng (defLat.toFixed(4),defLong.toFixed(4));
  
//  Create a new viewpoint bound
var bounds{$c} = new google.maps.LatLngBounds ();
//  Go through each...
{if $skip!=1}
for (var i = 0, LtLgLen = (LatLngList{$c}).length; i < LtLgLen; i++) {
  //  And increase the bounds to take this point
  if(LtLgLen>0){
  if((LatLngList{$c})[i]!=undefined){
   
  bounds{$c}.extend (LatLngList{$c}[i]);
  }
  }
}
{/if}
bounds{$c}.extend(LatLngTarget);

//zoom adjust
        var map{$c} = $("#mapgoogle{$c}").gmap("get", "map");
        map{$c}.fitBounds( bounds{$c} );
        
         //zoom adjust
{$c=$c+1}
    {/if}
{/foreach}
    
    


  {if isset($adress)}});{/if}//geotager if inserting



}, 500);
        });
        
        
        
        function findLocation(location, marker,mapa) {
     
	mapa.gmap('search', { 'location': location}, function(results, status) {
		if ( status === 'OK' ) {
                 {for $o=0 to $mapCount}
                    
                   //  console.log(location);
               // $('#mapgoogle{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].setPosition( new google.maps.LatLng( results[0].geometry.location.ib, results[0].geometry.location.jb ) );
                $('#mapgoogle{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].setPosition(location) ;
                  {/for}
			
				$.post("{$_subdomain}/Diary/setSessionGeoTag",{ 'lat':location.lat(),'lng':location.lng()});
					
			
		}
	});
}


function smoothZoom (map, max, cnt) {
    if (cnt >= max) {
            return;
        }
    else {
        z = google.maps.event.addListener(map, 'zoom_changed', function(event){
            google.maps.event.removeListener(z);
            self.smoothZoom(map, max, cnt + 1);
        });
        setTimeout(function(){ map.setZoom(cnt)}, 80); // 80ms is what I found to work well on my system -- it might not work well on all systems
    }
}
{$c=0}

var zoomlevel=10;
function zoomAllOut(){

 zoomlevel=zoomlevel-1;
{for $o=0 to $mapCount}
   // console.log($('#mapgoogle{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].getPosition());
    $('#mapgoogle{$o+1}').gmap('option', 'zoom', zoomlevel) ; 
    $('#mapgoogle{$o+1}').gmap('option', 'center', $('#mapgoogle{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].getPosition()) ; 
{/for}
}
function zoomAllIn(){
 zoomlevel=zoomlevel+1;
{for $o=0 to $mapCount}
    
   
     $('#mapgoogle{$o+1}').gmap('option', 'zoom', zoomlevel) ; 
   
    $('#mapgoogle{$o+1}').gmap('option', 'center', $('#mapgoogle{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].getPosition()) ; 
{/for}
}
        </script>
        <style>
        .btnStandard{
            float:none;
            }
        </style>
    </head>
    
    <body>
         {$o=0}
        {if isset($slotsType[$dates[$o]|date_format:"%d%m%Y"])}
      {$o=0}
                    <input {if isset($slotsType[$dates[$o]|date_format:"%d%m%Y"])}onclick="insertApp('{$dates[$o]|date_format:"%Y-%m-%d"}','{$slotsType[$dates[$o]|date_format:"%d%m%Y"]}');"{else}onclick="alert('Unfortunately you are unable to insert this appointment since you do not have any engineers available with the required skill on the selected day.'){/if};" type="checkbox" name="selectDay"> <span style="font-size:16px">{$dates[$o]|date_format:"%A %d %B"}</span>
                    <span style="font-size:16px;float:right">Slots left:    {if isset($slotsType[$dates[$o]|date_format:"%d%m%Y"])}{$slots[$dates[$o]|date_format:"%d%m%Y"]} {$slotsType[$dates[$o]|date_format:"%d%m%Y"]}{else}---{/if}</span>
                    <button class="btnStandard" onclick="showMultiMap();">Show MultiMaps</button>
        {/if}
                    <div id="mapgoogle1" class="multimap" style="width:100%;height:100%;border: 1px solid black"></div>
               
            
    </body>
</html>