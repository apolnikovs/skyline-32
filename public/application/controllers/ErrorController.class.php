<?php

require_once('CustomSmartyController.class.php');
require_once('Functions.class.php');

class ErrorController extends CustomSmartyController {
        public $config; 
    
        public function __construct() { 
        
        parent::__construct();   

       /* ==========================================
        * Read Application Config file.
        * ==========================================
        */
        $this->config = $this->readConfig('application.ini');
        
        /* ==========================================
         * initialise Session Model
         * ==========================================
         */
            
        $this->session = $this->loadModel('Session'); 
        
        //$this->smarty->assign('_theme', 'skyline');
        
        if (isset($this->session->UserID)) {
            
            $user_model = $this->loadModel('Users');
            $user_model->debug = false; // disable model debug for Error Handling Routine.
            $this->user = $user_model->GetUser( $this->session->UserID );
            
            
            
            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            
            $topLogoBrandID = $this->user->DefaultBrandID;
            $this->smarty->assign('_theme', $this->user->Skin);
            
        } else {
            
            $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');  
            $this->smarty->assign('logged_in_branch_name', '');
            $this->smarty->assign('_theme', 'skyline');
            
        }
        
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);
    }
    
    public function indexAction($args) {
                
        if ($args['Error'] == 'Session Expired') {
            // If User's configured Inactivity Period has been exceeded redirect user to Login Page....
            $this->redirect('LoginController', 'indexAction');
        } else {
            // Otherwise log the error and display the Error Page....
            $message = 'Unexpected Error:';
            if (isset($this->session->UserID)) {               
                if(isset($this->user->BranchName)) {
                    $message .= "\nBranch: " . $this->user->BranchName;
                }
                $message .= "\nUser: " . $this->user->Name;
            }
            $message .= "\nIP Address: " . Functions::getIPAddress();
            $message .= "\nUser Agent: " . isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
            foreach ($args as $key => $value) {
                $message .= "\n" . $key . ' = ' . $value;
            }
            $this->log($message, 'Errors_');

            //$this->smarty->assign('page_id', 999);
            $this->smarty->Assign('script_enabled', $this->session->SCRIPT);
            $this->smarty->assign('Title', 'Error');
            $this->smarty->assign('Error', $args);

            if ($args['Controller'] == 'AppController') {
                $this->smarty->display('app/Error.tpl');
            } else {
                $this->smarty->display('Error.tpl');
            }
        }
    }
}

?>
