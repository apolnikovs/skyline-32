# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.181');

# ---------------------------------------------------------------------- #
# Modify table "brand"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `brand` ADD COLUMN `CustomerTermsConditions` TEXT NULL DEFAULT NULL AFTER `ModifiedDate`;

 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.182');
