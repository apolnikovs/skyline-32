# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-09-12 09:41                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.77');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `client` DROP FOREIGN KEY `user_TO_client`;

ALTER TABLE `client` DROP FOREIGN KEY `service_provider_TO_client`;

ALTER TABLE `client` DROP FOREIGN KEY `branch_TO_client`;

ALTER TABLE `service_type` DROP FOREIGN KEY `brand_TO_service_type`;

ALTER TABLE `service_type` DROP FOREIGN KEY `job_type_TO_service_type`;

ALTER TABLE `service_type` DROP FOREIGN KEY `user_TO_service_type`;

ALTER TABLE `job` DROP FOREIGN KEY `network_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `branch_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `product_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `manufacturer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `job_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `status_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `model_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job_ModifiedUser`;

ALTER TABLE `network_client` DROP FOREIGN KEY `client_TO_network_client`;

ALTER TABLE `client_branch` DROP FOREIGN KEY `client_TO_client_branch`;

ALTER TABLE `service_type_alias` DROP FOREIGN KEY `client_TO_service_type_alias`;

ALTER TABLE `service_type_alias` DROP FOREIGN KEY `service_type_TO_service_type_alias`;

ALTER TABLE `unit_client_type` DROP FOREIGN KEY `client_TO_unit_client_type`;

ALTER TABLE `user` DROP FOREIGN KEY `client_TO_user`;

ALTER TABLE `audit` DROP FOREIGN KEY `job_TO_audit`;

ALTER TABLE `part` DROP FOREIGN KEY `job_TO_part`;

ALTER TABLE `appointment` DROP FOREIGN KEY `job_TO_appointment`;

ALTER TABLE `contact_history` DROP FOREIGN KEY `job_TO_contact_history`;

ALTER TABLE `brand` DROP FOREIGN KEY `client_TO_brand`;

ALTER TABLE `product` DROP FOREIGN KEY `client_TO_product`;

ALTER TABLE `unit_pricing_structure` DROP FOREIGN KEY `client_TO_unit_pricing_structure`;

ALTER TABLE `status_history` DROP FOREIGN KEY `job_TO_status_history`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `service_type_TO_central_service_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `client_TO_town_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `service_type_TO_town_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `service_type_TO_postcode_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `client_TO_postcode_allocation`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `client_TO_central_service_allocation`;

ALTER TABLE `bought_out_guarantee` DROP FOREIGN KEY `client_TO_bought_out_guarantee`;

ALTER TABLE `network` DROP FOREIGN KEY `client_TO_network`;

ALTER TABLE `claim_response` DROP FOREIGN KEY `job_TO_claim_response`;

# ---------------------------------------------------------------------- #
# Modify table "client"                                                  #
# ---------------------------------------------------------------------- #

ALTER TABLE `client` ADD COLUMN `TradeSpecificCallTypes` ENUM('Y','N') NOT NULL DEFAULT 'N';

ALTER TABLE `client` ADD COLUMN `TradeSpecificJobTypes` ENUM('Y','N') NOT NULL DEFAULT 'N';

ALTER TABLE `client` ADD COLUMN `ClientShortName` VARCHAR(9);

ALTER TABLE `client` MODIFY `TradeSpecificCallTypes` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `TradeSpecificManufacturers`;

ALTER TABLE `client` MODIFY `TradeSpecificJobTypes` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `TradeSpecificCallTypes`;

ALTER TABLE `client` MODIFY `ClientShortName` VARCHAR(9) AFTER `EnableProductDatabase`;

# ---------------------------------------------------------------------- #
# Modify table "service_type"                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_type` ADD COLUMN `Chargeable` ENUM('Y','N') NOT NULL DEFAULT 'N';

ALTER TABLE `service_type` ADD COLUMN `Warranty` ENUM('Y','N') NOT NULL DEFAULT 'N';

ALTER TABLE `service_type` ADD COLUMN `DateOfPurchaseForced` ENUM('N','B','C') NOT NULL DEFAULT 'N';

ALTER TABLE `service_type` ADD COLUMN `OverrideTradeSettingsPolicy` ENUM('Y','N') NOT NULL DEFAULT 'N';

ALTER TABLE `service_type` ADD COLUMN `ForcePolicyNoAtBooking` ENUM('Y','N') NOT NULL DEFAULT 'N';

ALTER TABLE `service_type` ADD COLUMN `OverrideTradeSettingsModel` ENUM('Y','N') NOT NULL DEFAULT 'N';

ALTER TABLE `service_type` ADD COLUMN `ForceModelNumber` ENUM('Y','N') NOT NULL DEFAULT 'N';

ALTER TABLE `service_type` MODIFY `Chargeable` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `Priority`;

ALTER TABLE `service_type` MODIFY `Warranty` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `Chargeable`;

ALTER TABLE `service_type` MODIFY `DateOfPurchaseForced` ENUM('N','B','C') NOT NULL DEFAULT 'N' AFTER `Warranty`;

ALTER TABLE `service_type` MODIFY `OverrideTradeSettingsPolicy` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `DateOfPurchaseForced`;

ALTER TABLE `service_type` MODIFY `ForcePolicyNoAtBooking` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `OverrideTradeSettingsPolicy`;

ALTER TABLE `service_type` MODIFY `OverrideTradeSettingsModel` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `ForcePolicyNoAtBooking`;

ALTER TABLE `service_type` MODIFY `ForceModelNumber` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `OverrideTradeSettingsModel`;

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD COLUMN `ReceiptNo` VARCHAR(40);

ALTER TABLE `job` ADD COLUMN `ColAddCompanyName` VARCHAR(100);

ALTER TABLE `job` ADD COLUMN `ColAddBuildingNameNumber` VARCHAR(40);

ALTER TABLE `job` ADD COLUMN `ColAddStreet` VARCHAR(40);

ALTER TABLE `job` ADD COLUMN `ColAddLocalArea` VARCHAR(40);

ALTER TABLE `job` ADD COLUMN `ColAddTownCity` VARCHAR(60);

ALTER TABLE `job` ADD COLUMN `ColAddCountyID` INTEGER;

ALTER TABLE `job` ADD COLUMN `ColAddCountryID` INTEGER;

ALTER TABLE `job` ADD COLUMN `ColAddPostcode` VARCHAR(10);

ALTER TABLE `job` MODIFY `ProductLocation` ENUM('Customer','Branch','Service Provider','Supplier','other');

CREATE INDEX `IDX_job_ColAddCountyID_FK` ON `job` (`ColAddCountyID`);

CREATE INDEX `IDX_job_ColAddCountryID_FK` ON `job` (`ColAddCountryID`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `client` ADD CONSTRAINT `user_TO_client` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client` ADD CONSTRAINT `service_provider_TO_client` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `client` ADD CONSTRAINT `branch_TO_client` 
    FOREIGN KEY (`DefaultBranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `service_type` ADD CONSTRAINT `brand_TO_service_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `service_type` ADD CONSTRAINT `job_type_TO_service_type` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `service_type` ADD CONSTRAINT `user_TO_service_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `job` ADD CONSTRAINT `product_TO_job` 
    FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

ALTER TABLE `job` ADD CONSTRAINT `service_type_TO_job` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `job_type_TO_job` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `model_TO_job` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `county_TO_job` 
    FOREIGN KEY (`ColAddCountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `job` ADD CONSTRAINT `country_TO_job` 
    FOREIGN KEY (`ColAddCountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `network_client` ADD CONSTRAINT `client_TO_network_client` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `client_TO_client_branch` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `client_TO_service_type_alias` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `service_type_TO_service_type_alias` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `unit_client_type` ADD CONSTRAINT `client_TO_unit_client_type` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `user` ADD CONSTRAINT `client_TO_user` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `audit` ADD CONSTRAINT `job_TO_audit` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `part` ADD CONSTRAINT `job_TO_part` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `job_TO_contact_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `brand` ADD CONSTRAINT `client_TO_brand` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `product` ADD CONSTRAINT `client_TO_product` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `client_TO_unit_pricing_structure` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `status_history` ADD CONSTRAINT `job_TO_status_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_type_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `client_TO_town_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_type_TO_town_allocation` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_type_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `client_TO_postcode_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `client_TO_central_service_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `client_TO_bought_out_guarantee` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `network` ADD CONSTRAINT `client_TO_network` 
    FOREIGN KEY (`DefaultClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `claim_response` ADD CONSTRAINT `job_TO_claim_response` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.78');
