# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.285');

# ---------------------------------------------------------------------- #
# Add viamente_end_day_history table     								 #
# ---------------------------------------------------------------------- # 
ALTER TABLE sp_engineer_day_location ADD GeoTag ENUM('EngineerLocation', 'REMOTE ENG: CONFIRM ARRIVAL', 'REMOTE ENG: CONFIRMED CUSTOMER', 'REMOTE ENG: CONFIRMED UNIT', 'REMOTE ENG: CONFIRMED WARRANTY', 'REMOTE ENG: CONFIRM DEPARTURE', 'REMOTE ENG: OUT CARD LEFT', 'REMOTE ENG: OUT CARD IMAGE');

ALTER TABLE sp_engineer_day_location ADD AppointmentID INT(11);

ALTER TABLE sp_engineer_day_location ADD ContactHistoryID INT(11);




# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.286');
